package com.example.listmaker

import android.os.Parcel
import android.os.Parcelable

//Entre parentesis constructor primario, si no se envian task el array se inicializa de cero
//en kotrlin se puede dar valor por defecto a las variables si no se envian.

//key , value
//Parcelabe es el serializable
class TaskList (val name: String, val tasks: ArrayList<String> = ArrayList()): Parcelable{

    constructor(parcel: Parcel) : this(
        //!! indica que siempre se va enviar un valor.
        //?: si falla envía lo que tiene a la derecha
        parcel.readString()?:"Generic List",
        //Si falla crea una lsita vacía
        parcel.createStringArrayList() ?: ArrayList()
    ) {
    }
    //guarda y descopone la liste en el parcel
    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
        //Guarda el arreglo de strings
        parcel.writeStringList(tasks)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<TaskList> {
        override fun createFromParcel(parcel: Parcel): TaskList {
            return TaskList(parcel)
        }

        override fun newArray(size: Int): Array<TaskList?> {
            return arrayOfNulls(size)
        }
    }


}