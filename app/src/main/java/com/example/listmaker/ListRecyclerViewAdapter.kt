package com.example.listmaker

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
//<>Generics, permiten transformar o hacer una clase que permita recibir cualquier tipo de dato.

interface ListSelectionListener {
    fun listItemSelected(list: TaskList)
}

class ListRecyclerViewAdapter(val lists: ArrayList<TaskList> = ArrayList(),
                              val selectionListener: ListSelectionListener)  : RecyclerView.Adapter<ListViewHolder>() {



    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.view_holder,parent, false)
        return ListViewHolder(view)
    }

    override fun getItemCount(): Int {
        return lists.size  //To change body of created functions use File | Settings | File Templates.
    }

    fun addList(list: TaskList){
        lists.add(list)

        //manda señal al recycle view de que tiene que volver pedir el data al adaptar para sabe que se cambiaron,
        //dibuja solo el item añadido no dibuja todo
        notifyItemInserted(lists.size -1 )
    }

    override fun onBindViewHolder(holder: ListViewHolder, position: Int) {
        holder.listItemId.text = (position + 1).toString()
        holder.listItemTitle.text = lists[position].name

        holder.itemView.setOnClickListener{
          selectionListener.listItemSelected(lists[position])
        }
    }

}