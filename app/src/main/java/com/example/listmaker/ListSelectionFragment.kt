package com.example.listmaker

import android.content.Context
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView


class ListSelectionFragment : Fragment(), ListSelectionListener {

    private var listener: OnFragmentInteractionListener? = null

    lateinit var listsRecyclerView: RecyclerView
    //Instanciar data manager
    lateinit var listDataManager: ListDataManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_list_selection, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val lists = listDataManager.readLists()

        //si existe la vista ejecuta lo de adentro
        view?.let {
            listsRecyclerView = it.findViewById(R.id.list_recyclerview)
            //listar los items de forma vertical o horizontal
            listsRecyclerView.layoutManager = LinearLayoutManager(activity)
            listsRecyclerView.adapter = ListRecyclerViewAdapter(lists,this)

        }
    }

    override fun listItemSelected(list: TaskList) {
        listener?.onListItemSelected(list)
    }



    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnFragmentInteractionListener) {
            listener = context
            listDataManager = ListDataManager(context)
        } else {
            throw RuntimeException(context.toString() + " must implement OnFragmentInteractionListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface OnFragmentInteractionListener {
        fun onListItemSelected(list: TaskList)

    }

    fun  addList(list: TaskList){
        //captura el adaptaer del recyclvie y ejecuta la funcion addlist
        listDataManager.saveList(list)
        val adapter =  listsRecyclerView.adapter as ListRecyclerViewAdapter
        adapter.addList(list)
    }

    fun saveList(list: TaskList){
        listDataManager.saveList(list)
        updateLists()
    }

    private fun updateLists(){
        val lists = listDataManager.readLists()
        listsRecyclerView.adapter = ListRecyclerViewAdapter(lists, this)
    }

    companion object {

        fun newInstance():ListSelectionFragment{
            val fragment = ListSelectionFragment()
            return fragment
        }
    }
}
