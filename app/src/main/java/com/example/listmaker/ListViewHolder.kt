package com.example.listmaker

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
//Herencia, constructor primario entre parentesis, inica el construcutor del padre con la misma vista
class ListViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
    //
    val listItemId = itemView.findViewById<TextView>(R.id.item_id)
    val listItemTitle = itemView.findViewById<TextView>(R.id.item_title)
}